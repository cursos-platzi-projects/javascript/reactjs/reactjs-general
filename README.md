# ReactJs-General

It is the first project's course I did to learn react js

# Knowledge

constructor(){
// Enlazo (bind) eventos y/o inicializo estado
/*
    Metodo llamado antes de que el componente sea montado 
    (El componente aun no se ve)
    1- Podemos iniciar el estado
    2- Enlazar (bind) de eventos
    3- Es el primer metodo que se llama al instanciar un componente
*/
}
componentWillMount(){
// Se ejecuta antes de montar el componente
// Se podría usar para hacer un setState()
/*
    Metodo llamado inmediatamente antes de que el componente se vaya a montar
    (El componente aun no se ve)
    1- Puedes hacer un setState()
    2- No hagas llamados a un API o suscripción a eventos
*/
}
render(){
// Contiene todos los elementos a renderizar
// podrías usarlo para calcular propiedades (ej: concatenar una cadena)
/*
    Contiene todos los elementos a renderizar
    (Estructura del componente)
    1- Contiene jsx en el return
    2- Puedes calcular propiedades nCompleto = name + lastName
*/
}
componentDidMount(){
//Solo se lanza una vez
//Ideal para llamar a una API, hacer un setInteval, etc
/*
    Metodo llamado luego de montarse el componente
    (El componente ya esta en pantalla)
    1- Solo se lanza una vez
    2- Enlazar (bind) de eventos
    3- Es el primer metodo que se llama al instanciar un componente
*/
}

//Actualización:
componentWillReceiveProps(){
//Es llamado cuando el componente recibe nuevas propiedades.
/*
    1- Metodo llamado al recibir nuevas propiedades
    2- Sirve para actualizar el estado con base a las nuevas propiedades
*/
}
shouldComponentUpdate(){
//Idea para poner una condición y  si las propiedades que le llegaron anteriormente
// eran las mismas que tenia retornar false para evitar re-renderear el componente
/*
    1- Metodo que condiciona si el componente se debe de volver a renderizar
    2- Utilizado para optimizar el rendimiento
*/
}
componentWillUpdate(){
//metodo llamado antes de re-renderizar el componente si shouldComponentUpdate devolvió true
/*
    1- Metodo llamado antes de re-renderizar un componente
    2- Utilizado para optimizar el rendimiento
*/
}

// re-render si es necesario...

componentDidUpdate(){
//Método llamado luego del re-render
}
componentWillUnmount(){
//Método llamado antes de desmontar el componente
//1- Metodo llamado antes de que el componente sea retirado de la aplicacion
}
componentDidCatch(){
// Si ocurre algún error, lo capturo desde acá:
/*
    1- Si ocurre algun error al renderizar el componente este metodo es invocado
    2- EL manejo de errores solo ocurre en componentes hijos
*/
}