import React, { Component } from 'react';
import './video.css';

class Video extends Component {
	togglePlay() {
		if (this.props.pause) {
			//Metodo play es propiamente del elemento que tiene por defecto en el navegador
			this.video.play()
		} else {
			//Metodo pause es propiamente del elemento que tiene por defecto en el navegador
			this.video.pause()
		}
	}
	//Metodo que recibe nuevas propiedades
	componentWillReceiveProps(nextProps) {
		if (nextProps.pause !== this.props.pause) {
			this.togglePlay();
		}
	}
	//Hace referencia al elemento <video>
	setRef = element => {
		this.video = element
	}
	render() {
		const {
      handleLoadedMetadata,
      handleTimeUpdate,
      handleSeeking,
      handleSeeked
    } = this.props;

		return (
		<div className="Video">
			<video
	          autoPlay={this.props.autoplay}
	          src={this.props.src}
	          ref={this.setRef}
	          onLoadedMetadata={handleLoadedMetadata}
	          onTimeUpdate={handleTimeUpdate}
	          onSeeking={handleSeeking}
	          onSeeked={handleSeeked}
	        />
		</div>
		)
	}
}

export default Video;