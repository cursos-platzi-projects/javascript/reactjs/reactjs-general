import React from 'react';
import './video-player-controls.css';

function VideoPlayerContols(props) {
	return (
		<div className="VideoPlayerControls">
			{props.children}
		</div>
	)
}

export default VideoPlayerContols;