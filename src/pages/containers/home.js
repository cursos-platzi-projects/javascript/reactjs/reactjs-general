import React, { Component } from 'react';
import HomeLayout from '../components/home-layout';
import Categories from '../../categories/components/categories.js';
import Related from '../components/related';
import ModalContainer from '../../widgets/containers/modal';
import Modal from '../../widgets/components/modal';
import HandleError from '../../../error/containers/handle-error';
import VideoPlayer from '../../player/containers/video-player';

class Home extends Component {
	state = {
		modalVisible: false,
		//handleError: false,
	}
	handleOpenModal = (media) => {
		this.setState({
			modalVisible: true,
			media
		})
	}
	/*componentDidCatch(error,info) {
		this.setState({
			handleError: true,
		})
	}*/
	handleCloseModal = (event) => {
		this.setState({
			modalVisible: false,
		})
	}
	render() {
		/*if (this.state.handleError) {
			return <p>OHHH hay un error :(</p>
		}*/
		return(
			<HandleError>
			  <HomeLayout>
				<Related />
				<Categories 
					categories={this.props.data.categories} 
					handleOpenModal={this.handleOpenModal}
				/>
				{
				  //Esto es si la condicion solamente se cumple
				  //En dado caso de tener dos opciones es con "? :"
				  this.state.modalVisible &&
				  <ModalContainer>
				    <Modal
				      handleClick={this.handleCloseModal}
				    >
							<VideoPlayer 
								autoplay
								src={this.state.media.src}
								title={this.state.media.title}
							/>
					  <h1>Esto es un portal</h1>
				    </Modal>
				  </ModalContainer>
				}
			  </HomeLayout>
			</HandleError>
		)
	}
}

export default Home