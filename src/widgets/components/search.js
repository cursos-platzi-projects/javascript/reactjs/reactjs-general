import React from 'react';
import './search.css';

//Tipica funcion
/*function Search(props) {
	return (
		<form action=""></form>
	)
}*/

//De esta manera haces una funcion que retorne algo sin ponerle el return
const Search = (props) => (
	<form 
	  className="Search"
	  onSubmit={props.handleSubmit}
	>
	  <input 
	    ref={props.setRef}
	    type="text" 
	    placeholder="Busca tu video favorito"
	    className="Search-input"
	    name="seach"
	    onChange={props.handleChange}
	    value={props.value}
	  />
	</form>
)

export default Search;