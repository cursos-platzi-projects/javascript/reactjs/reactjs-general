//import React, { Component } from 'react';
import React, {PureComponent} from 'react'
import PropTypes from 'prop-types';
import './media.css'
//class Media extends Component {
class Media extends PureComponent {
	//Asi es la forma en la EcmaScript 7
	/*state = {
		author: 'Gianni Alvarados'
	}*/
	//Asi es la forma en la EcmaScript 2015
	/*constructor(props) {
		super(props)
		this.state = {
			author: props.author
		}
		//this.handleClick = this.handleClick.bind(this);
	}*/
	//Funcion cuando haces un click, la funcione la puedes nombrar de cualquier nombre
	handleClick = (event) => {
		/*console.log(this.props.cover);
		//Con este condigo se cambia el valor lo que le habias pasado como parametro
		this.setState({
			author: 'Ricardo Celis',
		})*/
		this.props.openModal(this.props);
	}
	render() {
		const styles = {
			container: {
				fontSize: 14,
				backgroundColor: 'white',
				color: '#44546b',
				cursor: 'pointer',
				width: 260,
				border: '1px solid red'
			}
		}
		return (
			<div className="Media" onClick={this.handleClick}>
			  <div className="Media-cover">
			    <img 
			      src={this.props.cover}
			      alt=""
			      width={260}
			      height={160}
			      className="Media-image"
			    />
			    <h3 className="Media-title">{this.props.title}</h3>
			    <p className="Media-author">{this.props.author}</p>
			  </div>
			</div>
		)
	}
}

Media.propTypes = {
	cover: PropTypes.string,
	title: PropTypes.string.isRequired,
	author: PropTypes.string,
	type: PropTypes.oneOf(['video', 'audio']),
}

export default Media;